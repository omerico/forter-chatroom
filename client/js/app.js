let app = angular.module('app', [])

app.factory('dataServ', ['$http', $http => {
    return {
        get: function() {
            return $http.get('/data')
        }
    }
}])

app.controller('appController', ['$scope', 'dataServ', ( $scope, Data ) => {
    $scope.funnyStuff = { question: '', answer: '' }

    Data.get().success(resp => {
        $scope.funnyStuff = resp
    })
}])